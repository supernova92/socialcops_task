# SocialCops Task

### Problem Statement: [here](https://quip.com/AgyEAT91HAfS)

### Demo Link: [here](https://codeaffair.com/demo/social_cops/)

Tech Used:

 * __VueJS__: Not only because it was mentioned in the task, but I enjoy working on Vuejs myself. 
 * __Bootstrap 4__: I could have gone without using it, but it appeared to me that you might want to know that I am comfortable with the stack that helps in collaboration. Plus version 4 supports __Flexbox__ which is a win.
 * __Axios Library__: Simply to make XHR requests. In the Vue community, Axios and Vue-resource are pretty popular for the same.
 * __ChartJS__: To display charts.
 * Fonts: __Roboto Condensed__ and Some __FontAwesome__ glyphs

### A note on Browser Compatibility:
I have tested the working of the task in the following browsers:

* Windows 10
	* Firefox 55.0.3
	* 60.0.3112.113
	* Firefox 56(Beta)
	* Edge 14
	* Edge 15

* Windows 8.1
	* Firefox 55
	* Chrome 60

* Windows 7
	* Firefox 55
	* Chrome 60

* OS X Sierra
	* Firefox 55
	* Chrome 60	

* OS X Yosemite
	* Chrome 60

* Android 7.1
	* Chrome

* iOS 7
	* Safari
	* Chrome

_Note_: I don't own an iPhone, so whatever virtual emulators I could find, I tested on them.

Since, the code uses a little ES6 here and there and Flexbox, so the task should be compatible with any browser that runs these 2 decently.
I could increase the support by using a transpiler and Box model, but I figured you might want to know that I am comfortable in working with these models.

### Responsiveness:
I have tested the task upto __450px__ viewport size in both physical and virtual devices. Any device which has a decent support of Flexbox should look similar to what I intended.

### Vue Components:
In most projects at a production level, one should use components as much as they can. I didn't use them in this task, as I took a judgement call whether to show you the fact that I can work without them as well. Nevertheless, if its too important I can rewrite the code including the components functionality.

### Loading Time:
Some of the steps that I took to reduce loading time:
* Used Uglified to compress and combine scripts
* Minified and combined CSS to reduce the number of HTTP requests.
* Losslessly compressed images
* Increased browser caching for static resources
* Used a local font instead of Google font(_people tend to say, its better to use Google Fonts, but in my experience, whenever your target audience has a lower bandwidth, don't use Google Fonts because, often the DNS lookup for fonts fail, while the page is loaded. This won't happen the font is served from the same server_)
* Instead of using complete FontAwesome which has hundreds of fonts, I picked the glyphs needed for my project and used Fontello to get only the required fonts and rules.
* __PageSpeed Insight Results__: [here](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fcodeaffair.com%2Fdemo%2Fsocial_cops%2F&tab=desktop)
* I don't believe that just to get a score of 100 I should change my architecture, if that is the ultimate goal, certain quirks can be made to achieve a 100 score, but as you may have noticed the only point left to score a 100 is the __"above-fold-content"__ which is not very useful in this project.

### Offline Use:
I intended to make the task for offline use by using __localstorage__ or something similar, but 1 of the CSV has a size of __13MB__ which is higher than what browsers allow to store in localstorage. I could store only the relevant parts in the localstorage, but that appeared somewhat hacky to me so I didn't go through with it.

The function:

```javascript
process_array
```

This is not something I would use without some benchmark results, but in my early iterations of the code, it appeared to me that there was an initial lag in the browser due to so many records being processed. Using this function minimized the lag to a rough 2 seconds on my machine, but this has certainly given me much to research into. Thanks for that :+1:.

