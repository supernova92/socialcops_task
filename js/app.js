var app=new Vue({
	el:'#root',
	data:{
		loading_msg:'Almost ready...',
		data_loaded:false,
		matches_data:[],
		deliveries_data:[],
		visible_screen_index:0,
		selected_season:'2008',
		selected_team:{
			name:''
		},
		match_chart:null,
		batting_chart:null,
		bowling_chart:null,
		ticker_single_width:650
	},
	computed:{
		//array of seasons, simply
		seasons:function(){
			return [...new Set(this.array_column(this.matches_data,'season'))].reverse();
		},
		season_matches:function(){
			return this.matches_data.filter((match)=>{
				return match.season==this.selected_season;
			});
		},
		//list of teams playing in current season
		season_teams:function(){
			let teams=[];
			this.season_matches.map((match)=>{
				if(teams.indexOf(match.team1)==-1)
					teams.push(match.team1);
			});
			return teams;
		},
		//just to display the winner ribbon in the team list
		season_winner:function(){
			if(!this.data_loaded)
				return "";
			return this.season_matches[this.season_matches.length-1].winner;
		},
		//trivia to be displayed as ticker in the inside screen
		season_trivia:function(){
			let match_ids=this.array_column(this.season_matches,'id'),
				batsmen_stats=[],bowler_stats=[],
				num_4s=0,num_6s=0,dot_bowls=0,max_4s,max_6s,max_wickets,max_extras,max_dots;

			if(!this.deliveries_data.length)
				return "";

			this.deliveries_data.filter((delivery)=>{
				if(match_ids.indexOf(delivery.match_id)!==-1){
					if(batsmen_stats[delivery.batsman]===undefined){
						batsmen_stats[delivery.batsman]={
							runs:0,
							num_4s:0,
							num_6s:0
						};
					}
					if(bowler_stats[delivery.bowler]===undefined){
						bowler_stats[delivery.bowler]={
							runs:0,
							dots:0,
							extras:0,
							wickets:0
						};
					}
					if(max_4s===undefined){
						max_4s=delivery.batsman;
					}
					if(max_6s===undefined){
						max_6s=delivery.batsman;
					}
					if(max_extras===undefined){
						max_extras=delivery.bowler;
					}
					if(max_wickets===undefined){
						max_wickets=delivery.bowler;
					}
					if(max_dots===undefined){
						max_dots=delivery.bowler;
					}

					if(delivery.batsman_runs==4){
						num_4s++;
						batsmen_stats[delivery.batsman].num_4s++;

						if(batsmen_stats[max_4s].num_4s<batsmen_stats[delivery.batsman].num_4s)
							max_4s=delivery.batsman;
					}
					else if(delivery.batsman_runs==6){
						num_6s++;
						batsmen_stats[delivery.batsman].num_6s++;

						if(batsmen_stats[max_6s].num_6s<batsmen_stats[delivery.batsman].num_6s)
							max_6s=delivery.batsman;
					}

					if(delivery.total_runs==0){
						dot_bowls++;
						bowler_stats[delivery.bowler].dots++;

						if(bowler_stats[max_dots].dots<bowler_stats[delivery.bowler].dots)
							max_dots=delivery.bowler;
					}
					if(delivery.extra_runs>0){
						bowler_stats[delivery.bowler].extras+=parseInt(delivery.extra_runs);

						if(bowler_stats[max_extras].extras<bowler_stats[delivery.bowler].extra_runs)
							max_extras=delivery.bowler;
					}
					if(delivery.player_dismissed.length>0 && delivery.dismissal_kind!="run out"){
						bowler_stats[delivery.bowler].wickets++;

						if(bowler_stats[max_extras].wickets<bowler_stats[delivery.bowler].wickets)
							max_wickets=delivery.bowler;
					}
					
					batsmen_stats[delivery.batsman].runs+=parseInt(delivery.batsman_runs);
					bowler_stats[delivery.bowler].runs+=parseInt(delivery.total_runs);
					return true;
				}
				return false;
			});

			let trivia=[
				`In ${this.selected_season}, the maximum no. of 4s were hit by ${max_4s}(${batsmen_stats[max_4s].num_4s} 4s)`,
				`In ${this.selected_season}, the maximum no. of 6s were hit by ${max_6s}(${batsmen_stats[max_6s].num_6s} 6s)`,
				`In ${this.selected_season}, the maximum no. of wickets were taken by ${max_wickets}(${bowler_stats[max_wickets].wickets})`,
				`In ${this.selected_season}, the maximum no. of dot bowls were bowled by ${max_dots}(${bowler_stats[max_dots].dots})`,
				`In ${this.selected_season}, the maximum no. of extra runs were given by ${max_extras}(${bowler_stats[max_extras].extras} runs)`
			];

			// return trivia[Math.floor(Math.random()*trivia.length)];
			return trivia;
		}
	},
	mounted:function(){
		this.change_loading_msg();

		//fetch matches data
		axios.get('data/matches.csv').
			then((response)=>{
				let lines=response.data.split("\n"),
					header=lines[0].split(","),
					data=[];
				this.process_array(lines,1,(row,index)=>{
					if(!row.length)
						return;

					row=row.split(",");
					let obj={};

					for(let j=0;j<row.length;j++){
						obj[header[j]]=row[j];
					}
					data.push(obj);
				});
				this.matches_data=data;
			}).
			catch((err)=>{
				console.log(err);
			});

		//fetch deliveries data
		//and mark the data_loaded variable to true
		axios.get('data/deliveries.csv').
			then((response)=>{
				let lines=response.data.split("\n"),
					header=lines[0].split(","),
					data=[];
				this.process_array(lines,0,(row,index)=>{
					//before we return, mark data_loaded as true
					if(index==lines.length-1){
						this.data_loaded=true;
					}

					if(!row.length)
						return;
					row=row.split(",");
					let obj={};

					for(let j=0;j<row.length;j++){
						obj[header[j]]=row[j];
					}
					data.push(obj);
				});
				this.deliveries_data=data;
			}).
			catch((err)=>{
				console.log(err);
			});
	},
	methods:{
		//a little bit of cool messages to give a personalized feel from the begining
		change_loading_msg:function(){
			let msgs=[
				'Watching the highlights again...',
				'Prepaing charts for you...',
				'Loving Sachin <i class="fa fa-heart" style="color:#f33838;"></i>',
				'Looking at the top performers',
				'Almost ready...'
			],
			current_msg_index=msgs.indexOf(this.loading_msg),
			next_msg_index=current_msg_index==msgs.length-1?0:current_msg_index+1;

			this.loading_msg=msgs[next_msg_index];
			if(!this.data_loaded)
				setTimeout(()=>{
					this.change_loading_msg();
				},2000);
		},
		switch_screen:function(index){
			this.visible_screen_index=index;
		},
		select_season:function(year){
			this.selected_season=year;
			this.switch_screen(1);
			this.selected_team.name='';
			// this.select_team(this.season_teams[0]);
		},
		get_team_logo:function(team){
			return "images/logos/"+team.replace(new RegExp(" ", 'g'), "_").toLowerCase()+".jpg";
		},
		select_team:function(team){
			this.selected_team.name=team;
			this.get_team_stats_for_season();
		},
		//could have used as a computed prop as well, but felt more in place here
		get_team_matches:function(name){
			let matches=this.season_matches;
			return matches.filter((match)=>{
				return match.team1==name || match.team2==name;
			});
		},
		//when we click on a team, this function helps in getting the data from the graphs
		get_team_stats_for_season:function(){
			if(this.season_matches===undefined){
				console.log("matches undefined")
				return;
			}

			//match stats
			let matches=this.get_team_matches(this.selected_team.name),
				matches_played=0,matches_won=0,matches_lost=0,matches_tied=0;
			for(let i=0;i<matches.length;i++){
				let match=matches[i];
				if(match.team1==this.selected_team.name || match.team2==this.selected_team.name){
					matches_played++;

					if(match.result=="normal" && match.winner==this.selected_team.name)
						matches_won++;
					else if(match.result=="normal")
						matches_lost++;
					else if(mathces.result=="tie")
						matches_tied++;
				}
			}
			this.selected_team.matches_played=matches_played;
			this.selected_team.matches_won=matches_won;
			this.selected_team.matches_lost=matches_lost;
			this.selected_team.matches_tied=matches_tied;

			//batting and bowling stats
			let match_ids=this.array_column(this.season_matches,'id'),
				runs_scored=0,runs_in_4s=0,runs_in_6s=0,
				bowls_bowled=0,bowls_dots=0,bowls_boundaries=0;

			for(let i=1;i<this.deliveries_data.length;i++){
				let delivery=this.deliveries_data[i];
				if(match_ids.indexOf(delivery.match_id)==-1)
					continue;

				if(delivery.batting_team==this.selected_team.name){
					runs_scored+=parseInt(delivery.total_runs);

					if(delivery.batsman_runs=="4")
						runs_in_4s+=4;
					else if(delivery.batsman_runs=="6")
						runs_in_6s+=6;
				}

				if(delivery.bowling_team==this.selected_team.name){
					bowls_bowled++;
					if(delivery.total_runs=="0")
						bowls_dots++;
					
					else if(delivery.batsman_runs=="4" || delivery.batsman_runs=="6")
						bowls_boundaries++;
				}
			}
			this.selected_team.runs_scored=runs_scored;
			this.selected_team.runs_in_4s=runs_in_4s;
			this.selected_team.runs_in_6s=runs_in_6s;
			this.selected_team.bowls_bowled=bowls_bowled;
			this.selected_team.bowls_dots=bowls_dots;
			this.selected_team.bowls_boundaries=bowls_boundaries;

			let matches_data={
			        labels: ["Won", "Lost", "Tied"],
			        datasets: [{
			            label: '',
			            data: [matches_won,matches_lost,matches_tied],
			            backgroundColor: [
			                'rgba(255, 99, 132, 0.2)',
			                'rgba(54, 162, 235, 0.2)',
			                'rgba(255, 206, 86, 0.2)',
			            ],
			            borderColor: [
			                'rgba(54, 162, 235, 1)',
			                'rgba(255, 206, 86, 1)',
			                'rgba(75, 192, 192, 1)',
			            ],
			            borderWidth: 1
			        }]
			    },
			    bowling_data={
			        labels: ["Dot Balls", "Bowndaries", "Others"],
			        datasets: [{
			            // label: 'Total Balls Bowled',
			            data: [bowls_dots,bowls_boundaries,bowls_bowled - (bowls_dots+bowls_boundaries)],
			            backgroundColor: [
			                'rgba(255, 99, 132, 0.2)',
			                'rgba(54, 162, 235, 0.2)',
			                'rgba(255, 206, 86, 0.2)',
			            ],
			            borderColor: [
			                'rgba(54, 162, 235, 1)',
			                'rgba(255, 206, 86, 1)',
			                'rgba(75, 192, 192, 1)',
			            ],
			            borderWidth: 1
			        }]
			    },
			    batting_data={
			        labels: ["Runs in 4s", "Runs in 6s", "Others"],
			        datasets: [{
			            // label: 'Total runs Scored',
			            data: [runs_in_4s,runs_in_6s,runs_scored-(runs_in_4s+runs_in_6s)],
			            backgroundColor: [
			                'rgba(255, 99, 132, 0.2)',
			                'rgba(54, 162, 235, 0.2)',
			                'rgba(255, 206, 86, 0.2)',
			            ],
			            borderColor: [
			                'rgba(54, 162, 235, 1)',
			                'rgba(255, 206, 86, 1)',
			                'rgba(75, 192, 192, 1)',
			            ],
			            borderWidth: 1
			        }]
			    };
			    if(this.match_chart!=null)
			    	this.match_chart.destroy();
			    this.match_chart=this.display_chart("match_chart",matches_data);
			    if(this.batting_chart!=null)
			    	this.batting_chart.destroy();
			    this.batting_chart=this.display_chart("batting_chart",batting_data);
			    if(this.bowling_chart!=null)
			    	this.bowling_chart.destroy();
			    this.bowling_chart=this.display_chart("bowling_chart",bowling_data);
			
		},
		//function that displays/update the charts
		display_chart:function(id,data){
			let ctx = document.getElementById(id).getContext('2d');
			return new Chart(ctx, {
			    type: 'bar',
			    data: data,
			    options:{
			    	legend:{
			    		display:false
			    	}
			    }
			});
		},
		//using this function instead of for loop helps me in reducing the time the browser stays in non-responsive mode while tackling the big array
		process_array:function(arr,index,callback){
			let el,
				cur_time=+new Date(),
				end_time=cur_time+2000;
			do{
				el=arr[index];
				//process element here
				callback(el,index);
				index++;
				cur_time=+new Date();
			}while(cur_time<end_time && index<arr.length);
			if(index<arr.length)
				setTimeout(()=>{
					this.process_array(arr,index,callback);
				},500);
		},
		//not using prototype coz it's not recommended
		array_column:function(arr,key){
			let new_arr=[];
			arr.map((row)=>{
				new_arr.push(row[key]);
			});
			return new_arr;
		},
		//helper function
		change_date_format:function(str){
			let date=new Date(str);
			return this.str_pad_left(date.getDate(),"0",2) + "/" + this.str_pad_left(date.getMonth() + 1,"0",2) + "/" + date.getFullYear();
		},
		//helper function
		str_pad_left:function(str,char,length){
			return (char.repeat(length) + str).slice(-1*length);
		}

	}
});